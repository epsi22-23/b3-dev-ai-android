package fr.epsi.b3devai

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class SignUpActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        showBack()
        setHeaderTitle(getString(R.string.txt_signup))
        val buttonGoSignup = findViewById<Button>(R.id.buttonGoSignup)
        val editTextFirstName = findViewById<EditText>(R.id.editTextFirstName)
        editTextFirstName.setText(readSharedPref("firstName"))

        val editTextLastName = findViewById<EditText>(R.id.editTextLastName)
        editTextLastName.setText(readSharedPref("lastName"))

        val editTextPassword = findViewById<EditText>(R.id.editTextPassword)
        editTextPassword.setText(readSharedPref("password"))

        val editTextEmail = findViewById<EditText>(R.id.editTextEmail)
        editTextEmail.setText(readSharedPref("email"))

        val editTextAddress = findViewById<EditText>(R.id.editTextAddress)
        editTextAddress.setText(readSharedPref("address"))

        val editTextCity = findViewById<EditText>(R.id.editTextCity)
        editTextCity.setText(readSharedPref("city"))

        val editTextZipCode = findViewById<EditText>(R.id.editTextZipCode)
        editTextZipCode.setText(readSharedPref("zipCode"))

        buttonGoSignup.setOnClickListener(View.OnClickListener {
            writeSharedPref("firstName", editTextFirstName.text.toString())
            writeSharedPref("lastName", editTextLastName.text.toString())
            writeSharedPref("password", editTextPassword.text.toString())
            writeSharedPref("email", editTextEmail.text.toString())
            writeSharedPref("address", editTextAddress.text.toString())
            writeSharedPref("city", editTextCity.text.toString())
            writeSharedPref("zipCode", editTextZipCode.text.toString())

        })
    }

    fun writeSharedPref(key: String, value: String) {
        val sharedPreferences: SharedPreferences =
            getSharedPreferences("account", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun readSharedPref(key: String): String {
        val sharedPreferences: SharedPreferences =
            getSharedPreferences("account", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "").toString()
    }

}