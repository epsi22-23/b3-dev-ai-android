package fr.epsi.b3devai

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        showBack()
        setHeaderTitle(getString(R.string.txt_login))
        val editTextEmail = findViewById<EditText>(R.id.editTextEmail)
        val editTextPassword = findViewById<EditText>(R.id.editTextPassword)
        val buttonGoLogin = findViewById<Button>(R.id.buttonGoLogin)

        buttonGoLogin.setOnClickListener(View.OnClickListener {
            val txt = editTextEmail.text.toString() + "/" + editTextPassword.text

            Toast.makeText(application, txt, Toast.LENGTH_SHORT).show()
        })
    }
}