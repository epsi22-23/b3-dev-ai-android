package fr.epsi.b3devai

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class DetailsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val title = intent.extras?.getString("title")
        setHeaderTitle(title)
        showBack()

        val url = intent.extras?.getString("url")
        val imageViewNature=findViewById<ImageView>(R.id.imageViewNature)
        Picasso.get().load(url).into(imageViewNature);
    }
}