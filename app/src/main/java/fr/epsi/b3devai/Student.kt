package fr.epsi.b3devai

class Student(
    val name: String,
    val email: String,
    val phone: String,
    val city: String,
    val zipcode: String,
    val imgUrl: String
) {
}