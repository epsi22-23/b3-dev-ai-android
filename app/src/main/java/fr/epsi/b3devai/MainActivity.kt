package fr.epsi.b3devai

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setHeaderTitle(getString(R.string.txt_title_epsi))

        val buttonNature = findViewById<Button>(R.id.buttonNature)
        buttonNature.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, DetailsActivity::class.java)
            val url =
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROFiHOkKUHVBF3TcyU1NgawBlIV9mIoSGAuA&usqp=CAU"
            newIntent.putExtra("url", url)
            newIntent.putExtra("title", getString(R.string.txt_nature))

            startActivity(newIntent)
        })

        val buttonSpace = findViewById<Button>(R.id.buttonSpace)
        buttonSpace.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, DetailsActivity::class.java)
            val url =
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRj0AEwRdUSWfs2LPDlLKn9kI-KvverDKfy0w&usqp=CAU"
            newIntent.putExtra("url", url)
            newIntent.putExtra("title", getString(R.string.txt_space))
            startActivity(newIntent)
        })

        val buttonLogin = findViewById<Button>(R.id.buttonLogin)
        buttonLogin.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, LoginActivity::class.java)
            startActivity(newIntent)
        })

        val buttonSignUp = findViewById<Button>(R.id.buttonSignUp)
        buttonSignUp.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, SignUpActivity::class.java)
            startActivity(newIntent)
        })

        val buttonStudents = findViewById<Button>(R.id.buttonStudents)
        buttonStudents.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, StudentsActivity::class.java)
            startActivity(newIntent)
        })

        val buttonStudentsWS = findViewById<Button>(R.id.buttonStudentsWs)
        buttonStudentsWS.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application, StudentsWsActivity::class.java)
            startActivity(newIntent)
        })
    }
}