package fr.epsi.b3devai

import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class StudentsWsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_students)
        showBack()
        setHeaderTitle("Students WS")

        val students = arrayListOf<Student>()

        val recyclerViewStudents = findViewById<RecyclerView>(R.id.recyclerViewStudents)
        recyclerViewStudents.layoutManager = LinearLayoutManager(this)

        val studentAdapter = StudentAdapter(students)
        recyclerViewStudents.adapter = studentAdapter

        val okHttpClient: OkHttpClient = OkHttpClient.Builder().build()
        val mRequestURL = "https://www.ugarit.online/epsi/list.json"
        val request = Request.Builder()
            .url(mRequestURL)
            .get()
            .cacheControl(CacheControl.FORCE_NETWORK)
            .build()

        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("Epsi", "############## " + e.message.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                val data = response.body?.string()
                Log.e("Epsi", "################# response.code:" + response.code)
                if (data != null && response.code == 200) {
                    Log.e("Epsi", data)
                    val jsStudents = JSONObject(data)
                    val jsArrayStudents = jsStudents.getJSONArray("items")
                    val url =
                        "https://img.freepik.com/photos-gratuite/rendu-3d-fond-espace-planetes-abstraites-nebuleuse_1048-12994.jpg?w=740&t=st=1673970224~exp=1673970824~hmac=5db29203b7bcddbecb50f50b6bab3920b729055ce5b87aaccc3260362a801f19"
                    for (i in 0 until jsArrayStudents.length()) {
                        val js = jsArrayStudents.getJSONObject(i)
                        val student = Student(
                            js.optString("name", "Not found"),
                            js.optString("email", "Not found"),
                            js.optString("phone", "Not found"),
                            js.optString("city", "Not found"),
                            js.optString("zipcode", "Not found"),
                            js.optString("picture_url", url)
                        )
                        students.add(student)
                        runOnUiThread(Runnable {
                            studentAdapter.notifyDataSetChanged()
                        })
                    }
                }
            }
        })
    }
}